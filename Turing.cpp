#include <iostream>
#include <string>
#include <fstream>
#include <tuple>
#include <map>
#include <utility>

using namespace std;

void main(void)
{
	ifstream read;
	read.open("text.txt");

	string start = " ", final = " ", state1 = " ", letter1 = " ", state2 = " ", letter2 = " ", dir = " ";
	read >> start >> final;

	map <tuple <string, string>, tuple <string, string, string>> rules;
	map <tuple <string, string>, tuple <string, string, string>>::iterator itr;

	while (read)
	{
		read >> state1 >> letter1 >> state2 >> letter2 >> dir;
		rules[ make_tuple(state1, letter1)] = make_tuple(state2, letter2, dir);
	}

	read.close();

	string userInput = " ";
	cout << "Enter string: ";
	getline(cin, userInput);

	int i = 0;
	string currentState = start, leftTape = "", rightTape = userInput;
	letter1 = rightTape[i];

	while (true)
	{
		cout << leftTape << "(" << currentState << ")" << rightTape << endl;
		itr = rules.find(make_tuple(currentState, letter1));

		if ( currentState == final )
		{
			cout << "\nAccepted:" << endl;
			cout << "state = " << currentState << endl;
			cout << "tape = " << leftTape + rightTape << endl << endl;
			break;
		}
		if ( itr == rules.end())
		{
			cout << "\nCrashed:" << endl;
			cout << "state = " << currentState << endl;
			cout << "tape = " << leftTape + rightTape << endl << endl;
			break;
		}
		else
		{
			state2 = get<0>(itr->second);
			letter2 = get<1>(itr->second);
			dir = get<2>(itr->second);
		}

		if ( dir == "R" )
		{
			leftTape += letter2;
			if ( rightTape.length() > 1 )
			{
				rightTape = rightTape.substr(1);
				letter1 = rightTape[0];
			}
			else
			{
				rightTape = "B";
				letter1 = "B";
			}
			currentState = state2;
			
		}
		else
		{
			rightTape = letter2 + rightTape.substr(1);
			char ch = leftTape.back();
			rightTape = ch + rightTape;
			leftTape = leftTape.substr(0, leftTape.length()-1);
			currentState = state2;
			letter1 = ch;
		}
	}

	system("pause");
}